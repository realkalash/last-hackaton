package com.example.myexamen.utils;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {
    public static String getTimeFrom1970(String time) {

        long timesunRiseLong = (long) Double.parseDouble(time);

        Calendar sunriseCalendar = new GregorianCalendar();

        sunriseCalendar.setTimeInMillis(timesunRiseLong);
        Date sunriseDate = sunriseCalendar.getTime();

       String result = (String) DateFormat.format("HH:mm",
                sunriseDate);

        return result;
    }
}
