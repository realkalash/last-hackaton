package com.example.myexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.myexamen.Appi.WeathModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondActivity extends AppCompatActivity {
    @BindView(R.id.sa_descr)
    TextView descr;
    @BindView(R.id.sa_sunrise)
    TextView sunrise;
    @BindView(R.id.sa_sunset)
    TextView sanset;
    @BindView(R.id.sa_temperaturs)
    TextView temperatures;
    @BindView(R.id.sa_wind)
    TextView wind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);

        WeathModel weathModel = (WeathModel) getIntent().getParcelableExtra("EXTRA_MODEL");

        descr.setText(weathModel.getDescription());
        sunrise.setText("Sunrise: "+weathModel.getTimeDawn());
        sanset.setText("Sanset: " + weathModel.getTimeSunset());
        temperatures.setText(weathModel.getTemperature());
        wind.setText(weathModel.getSpeedWind()+ " mph");

    }
}
