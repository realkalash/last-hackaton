package com.example.myexamen;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.example.myexamen.Appi.ApiService;
import com.example.myexamen.Appi.Converter;
import com.example.myexamen.Appi.RequestModel;
import com.example.myexamen.Appi.WeathModel;
import com.example.myexamen.adapter.MyRecyclerAdapter;
import com.example.myexamen.database.MDatabase;
import com.example.myexamen.database.MyDataBaseDao;
import com.example.myexamen.utils.Utils;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements MyRecyclerAdapter.OnDrinkClickListener{

    MDatabase myDataBaseDao;
    List<WeathModel> weathModelList;
    MyDataBaseDao dao;

    Disposable subscribe;


    @BindView(R.id.city)
    TextView cityTextView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.dateAndTime)
    TextView dateAndTimeTextView;

    @BindView(R.id.sky)
    TextView skyTextView;

    @BindView(R.id.temperature)
    TextView temperatureTextView;

    @BindView(R.id.temperatureVariance)
    TextView temperatureVarianceTextView;

    @BindView(R.id.skyImageView)
    ImageView skyImageView;

    @BindView(R.id.sunRiseAndSetImageView)
    ImageView sunRiseAndSetImageView;

    @BindView(R.id.sunRiseTime)
    TextView sunRiseTime;

    @BindView(R.id.sunSetTime)
    TextView sunSetTime;

    @BindView(R.id.windSpeedImageView)
    ImageView windSpeedImageView;

    @BindView(R.id.windSpeedTextView)
    TextView windSpeedTextView;


    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        myDataBaseDao = Room
                .databaseBuilder(this, MDatabase.class, "database")
                .allowMainThreadQueries()
                .build();

        dao = myDataBaseDao.getDatabaseDao();

        if (dao.countAll() > 0) {
            subscribe = dao.selectAll()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<WeathModel>>() {

                        @Override
                        public void accept(List<WeathModel> weathModels) throws Exception {
                            weathModelList = weathModels;

                        }
                    });
        } else {
            ApiService.getAll("Kharkiv", "041a6b1d5a70f0cfc9baa490038250a4")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<RequestModel>() {
                        @Override
                        public void accept(RequestModel requestModel) throws Exception {
                            weathModelList = Converter.convertRequest(requestModel);
                            setTexts();

                            generateWeather();

                            MyRecyclerAdapter myRecyclerAdapter = new MyRecyclerAdapter(weathModelList, MainActivity.this, MainActivity.this);

                            recyclerView.setAdapter(myRecyclerAdapter);
                        }

                    });
        }

    }

    private void generateWeather() {
        weathModelList.add(new WeathModel("Clear",
                "Clear sky",
                "4:23",
                "5:25",
                "no",
                "8",
                "301",
                "27",
                "29"));

        weathModelList.add(new WeathModel("Clear",
                "Clear sky",
                "4:50",
                "5:30",
                "no",
                "8",
                "310",
                "25",
                "26"));


        weathModelList.add(new WeathModel("Clear",
                "Clear sky",
                "4:23",
                "5:25",
                "no",
                "8",
                "301",
                "27",
                "29"));

        weathModelList.add(new WeathModel("Clear",
                "Clear sky",
                "4:50",
                "5:30",
                "no",
                "8",
                "310",
                "25",
                "26"));


        weathModelList.add(new WeathModel("Clear",
                "Clear sky",
                "4:23",
                "5:25",
                "no",
                "8",
                "301",
                "27",
                "29"));
    }

    private void setTexts() {
        WeathModel weatherToday = weathModelList.get(0);

        String des = String.format("%s/%s %s", weatherToday.getMinTemperature(),
                weatherToday.getMaxTemperature(),
                weatherToday.getDescription());

        temperatureTextView.setText(weatherToday.getTemperature());
        temperatureVarianceTextView.setText(des);
        skyTextView.setText(weatherToday.getWeather());

        sunRiseTime.setText("Sunrise: " + Utils.getTimeFrom1970(weatherToday.getTimeDawn()));

        sunSetTime.setText("Sunset: " + Utils.getTimeFrom1970(weatherToday.getTimeSunset()));

        dateAndTimeTextView.setText(DateFormat.format("dd-MM-yyyy (HH:mm)",
                new Date().getTime()));

        windSpeedTextView.setText(weatherToday.getSpeedWind() + " mph");

        switch (weatherToday.getWeather()) {
            case "Clear":
                Glide.with(this).load(R.drawable.sunny).into(skyImageView);
                break;
            case "Cloudy":
                Glide.with(this).load(R.drawable.cloudy).into(skyImageView);
                break;
            case "Rain":
                Glide.with(this).load(R.drawable.rain).into(skyImageView);
                break;

        }


    }

    @Override
    protected void onDestroy() {
        subscribe.dispose();
        super.onDestroy();
    }

    @Override
    public void onItemClick(WeathModel weathModel) {
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        intent.putExtra("EXTRA_MODEL", weathModel);
        startActivity(intent);
    }
}
