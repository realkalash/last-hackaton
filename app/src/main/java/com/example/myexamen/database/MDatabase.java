package com.example.myexamen.database;


import android.os.Build;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.myexamen.Appi.WeathModel;


@Database(entities = {WeathModel.class}, version = 1)
public abstract class MDatabase extends RoomDatabase {

    public abstract MyDataBaseDao getDatabaseDao();

}
