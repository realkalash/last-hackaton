package com.example.myexamen.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.myexamen.Appi.WeathModel;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class MyDataBaseDao {
    @Insert
    public abstract void insertAll(List<WeathModel> filmsModels);

    @Query("SELECT * FROM WeathModel")

    public abstract Flowable<List<WeathModel>> selectAll();


    @Query("SELECT COUNT(*) FROM WeathModel")
    public abstract Integer countAll();

    @Query("DELETE FROM WeathModel")
    public abstract void removeAll();
}
