package com.example.myexamen.Appi;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class WeathModel implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    public int id;

    String weather;
    String description;
    String timeDawn;
    String timeSunset;
    String chanceRain;
    String speedWind;
    String temperature;

    String minTemperature;
    String maxTemperature;

    public WeathModel(String weather, String description, String timeDawn, String timeSunset, String chanceRain, String speedWind, String temperature, String minTemperature, String maxTemperature) {
        this.weather = weather;
        this.description = description;
        this.timeDawn = timeDawn;
        this.timeSunset = timeSunset;
        this.chanceRain = chanceRain;
        this.speedWind = speedWind;
        this.temperature = temperature;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
    }

    protected WeathModel(Parcel in) {
        id = in.readInt();
        weather = in.readString();
        description = in.readString();
        timeDawn = in.readString();
        timeSunset = in.readString();
        chanceRain = in.readString();
        speedWind = in.readString();
        temperature = in.readString();
        minTemperature = in.readString();
        maxTemperature = in.readString();
    }

    public static final Creator<WeathModel> CREATOR = new Creator<WeathModel>() {
        @Override
        public WeathModel createFromParcel(Parcel in) {
            return new WeathModel(in);
        }

        @Override
        public WeathModel[] newArray(int size) {
            return new WeathModel[size];
        }
    };

    public String getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(String minTemperature) {
        this.minTemperature = minTemperature;
    }

    public String getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(String maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getTemperature() {
        double temp = Double.parseDouble(temperature);


        double clearDouble = Math.round(temp) - 273;

        return String.format("%.0f C°", clearDouble);
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getTimeDawn() {
        return timeDawn;
    }

    public void setTimeDawn(String timeDawn) {
        this.timeDawn = timeDawn;
    }

    public String getTimeSunset() {
        return timeSunset;
    }

    public void setTimeSunset(String timeSunset) {
        this.timeSunset = timeSunset;
    }

    public String getChanceRain() {
        return chanceRain;
    }

    public void setChanceRain(String chanceRain) {
        this.chanceRain = chanceRain;
    }

    public String getSpeedWind() {
        return speedWind;
    }

    public void setSpeedWind(String speedWind) {
        this.speedWind = speedWind;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(weather);
        dest.writeString(description);
        dest.writeString(timeDawn);
        dest.writeString(timeSunset);
        dest.writeString(chanceRain);
        dest.writeString(speedWind);
        dest.writeString(temperature);
        dest.writeString(minTemperature);
        dest.writeString(maxTemperature);
    }
}
